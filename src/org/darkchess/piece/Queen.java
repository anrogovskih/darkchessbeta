package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Colors;

import java.util.ArrayList;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Queen extends Piece {

    public Queen(Colors color, int blockIndex, boolean isOnStartPosition) {
        super(color, blockIndex, isOnStartPosition, 4);
    }

    @Override
    public boolean isValidMove(int from, int to) {
        if (!Board.noPieceOnWay(from,to)) {
            return false;
        }
        if (Board.getBlocks()[to].getPiece() != null && Board.getBlocks()[from].getPiece().getColor() == Board.getBlocks()[to].getPiece().getColor())
        {
            return false;
        }

        int toFrom = Math.abs(to - from);

        return (toFrom % 8 == 0) || ((Board.isInLine(from, to)) && Board.isInLine(to, from)) || Board.isInDiagonal(to, from);
    }

    @Override
    public String getType() {
        return "Queen";
    }

    @Override
    public ArrayList<Integer> getVisibleBlocks(int position) {
        ArrayList<Integer> blocks = super.getVisibleBlocks(position);

        // diagonal
        int rightCorner = position;
        while (Board.isInLine(position, rightCorner + 1) && rightCorner < 63) rightCorner++;

        int leftCorner = position;
        while (Board.isInLine(leftCorner - 1, position) && leftCorner > 0) leftCorner--;

        for (int i = 1; i < (rightCorner + 1) - position; i++) {
            int pos = position + 9 * i;
            blocks.add(pos);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (rightCorner + 1) - position; i++) {
            int pos = position - 7 * i;
            blocks.add(position - 7 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (position + 1) - leftCorner ; i++) {
            int pos = position + 7 * i;
            blocks.add(position + 7 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (position + 1) - leftCorner ; i++) {
            int pos = position - 9 * i;
            blocks.add(position - 9 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        // vertical and horizontal
        for (int i = 1; i < 8; i++) {
            int pos = position + 8 * i;
            blocks.add(pos);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < 8; i++) {
            int pos = position - 8 * i;
            blocks.add(pos);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        int visiblePos = position + 1;
        while (Board.isInLine(position, visiblePos)) {
            blocks.add(visiblePos);
            if (visiblePos < 64 && visiblePos > -1) {
                if (Board.getBlocks()[visiblePos].getPiece() != null) break;
            }
            visiblePos++;
        }

        visiblePos = position - 1;
        while (Board.isInLine(visiblePos, position)) {
            blocks.add(visiblePos);
            if (visiblePos < 64 && visiblePos > -1) {
                if (Board.getBlocks()[visiblePos].getPiece() != null) break;
            }
            visiblePos--;
        }

        return blocks;
    }
}
