package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Colors;

import java.util.ArrayList;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Knight extends Piece {

    public Knight(Colors color, int blockIndex, boolean isOnStartPosition) {
        super(color, blockIndex, isOnStartPosition, 2);
    }

    @Override
    public boolean isValidMove(int from, int to) {

        if (Board.getBlocks()[to].getPiece() != null && Board.getBlocks()[from].getPiece().getColor() == Board.getBlocks()[to].getPiece().getColor())
        {
            return false;
        }
        int toFrom = Math.abs(to - from);

        return ((toFrom == 17) || (toFrom == 15) || (toFrom == 10) || ((toFrom == 6))) && (Board.numOfColumnsInBetween(from, to) < 4);
    }

    @Override
    public String getType() {
        return "Knight";
    }

    @Override
    public ArrayList<Integer> getVisibleBlocks(int position) {
        ArrayList<Integer> blocks = super.getVisibleBlocks(position);

        if (Board.numOfColumnsInBetween(position, position + 17) < 4) blocks.add(position + 17);
        if (Board.numOfColumnsInBetween(position, position - 17) < 4) blocks.add(position - 17);
        if (Board.numOfColumnsInBetween(position, position + 15) < 4) blocks.add(position + 15);
        if (Board.numOfColumnsInBetween(position, position - 15) < 4) blocks.add(position - 15);
        if (Board.numOfColumnsInBetween(position, position + 10) < 4) blocks.add(position + 10);
        if (Board.numOfColumnsInBetween(position, position - 10) < 4) blocks.add(position - 10);
        if (!Board.isInLine(position,position + 6)) blocks.add(position + 6);
        if (!Board.isInLine(position - 6, position)) blocks.add(position - 6);


        return blocks;
    }

}
