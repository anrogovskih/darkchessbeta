package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;

import java.util.ArrayList;

/**
 * Created by Анатолий on 16.02.2016.
 */

    public abstract class Piece {

        protected int power;
        protected Colors color;
        protected int blockIndex;
        protected boolean isFirstMove;

        public Piece(Colors color, int blockIndex, boolean isOnStartPosition, int power) {
            this.color = color;
            this.blockIndex = blockIndex;
            this.isFirstMove = isOnStartPosition;
            this.power = power;
        }

        public int getPower() {
            return power;
        }

        public abstract boolean isValidMove(int from, int to);

        public abstract String getType();

        public Colors getColor() {
            return color;
        }

        public int getBlockIndex() {
            return blockIndex;
        }

        public void setBlockIndex(int blockIndex) {
            this.blockIndex = blockIndex;
        }

        public boolean isFirstMove() {
            return isFirstMove;
        }

        public void setIsFirstMove(boolean isOnStartPosition) {
            this.isFirstMove = isOnStartPosition;
        }

        public ArrayList<Integer> getVisibleBlocks(int position) {
            ArrayList<Integer> blocks = new ArrayList<>();

            blocks.add(position);
            for (int i = 0; i < 64; i++) {
                if ((Board.numOfColumnsInBetween(position, i) < 2) && (Board.numOfLinesInBetween(position, i) < 2)){
                    blocks.add(i);
                }
            }
            return blocks;
        }
}
