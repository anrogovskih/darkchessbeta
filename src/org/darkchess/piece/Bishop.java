package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;

import java.util.ArrayList;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Bishop extends Piece {

    public Bishop(Colors color, int blockIndex, boolean isOnStartPosition) {
        super(color, blockIndex, isOnStartPosition, 2);
    }

    @Override
    public boolean isValidMove(int from, int to) {

        if (!Board.noPieceOnWay(from,to)) {
            return false;
        }
        if (Board.getBlocks()[to].getPiece() != null && Board.getBlocks()[from].getPiece().getColor() == Board.getBlocks()[to].getPiece().getColor())
        {
            return false;
        }
        return  Board.isInDiagonal(to, from);


    }

    @Override
    public String getType() {
        return "Bishop";
    }

    @Override
    public ArrayList<Integer> getVisibleBlocks(int position) {
        ArrayList<Integer> blocks = super.getVisibleBlocks(position);

        int rightCorner = position;
        while (Board.isInLine(position, rightCorner + 1) && rightCorner < 63) rightCorner++;

        int leftCorner = position;
        while (Board.isInLine(leftCorner - 1, position) && leftCorner > 0) leftCorner--;

        for (int i = 1; i < (rightCorner + 1) - position; i++) {
            int pos = position + 9 * i;
            blocks.add(pos);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (rightCorner + 1) - position; i++) {
            int pos = position - 7 * i;
            blocks.add(position - 7 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (position + 1) - leftCorner ; i++) {
            int pos = position + 7 * i;
            blocks.add(position + 7 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        for (int i = 1; i < (position + 1) - leftCorner ; i++) {
            int pos = position - 9 * i;
            blocks.add(position - 9 * i);
            if (pos < 64 && pos > -1) {
                if (Board.getBlocks()[pos].getPiece() != null) break;
            }
        }

        return blocks;
    }
}
