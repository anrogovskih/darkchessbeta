package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.general.Move;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class King extends Piece {
    public King(Colors color, int blockIndex, boolean isOnStartPosition) {
        super(color, blockIndex, isOnStartPosition, 5);
    }

    @Override
    public boolean isValidMove(int from, int to) {
        if (!Board.noPieceOnWay(from, to)) {
            return false;
        }

        if (Board.getBlocks()[to].getPiece() != null && Board.getBlocks()[from].getPiece().getColor() == Board.getBlocks()[to].getPiece().getColor()) {
            return false;
        }

        return ((Board.numOfColumnsInBetween(from, to) < 2) && (Board.numOfLinesInBetween(from, to) < 2))
                || isCastling(from, to);
    }

    @Override
    public String getType() {
        return "King";
    }

    public static void doCastling(Move move) {
        if ((Board.numOfColumnsInBetween(move.getFrom(), move.getTo()) == 2)
                && Board.getBlocks()[move.getTo()].getPiece().getType().equals("King")) {
            switch (move.getTo()) {
                case 2: {
                    Game.movePiece(new Move(0,3));
                    break;
                }
                case 6: {
                    Game.movePiece(new Move(7, 5));
                    break;
                }
                case 58: {
                    Game.movePiece(new Move(56,59));
                    break;
                }
                case 62: {
                    Game.movePiece(new Move(63,61));
                    break;
                }
            }
        }
    }

    public boolean isCastling(int from, int to) {
        if (isFirstMove() && Board.noPieceOnWay(from, to)) {
            switch (to) {
                case 2:
                    if (Board.getBlocks()[4].getPiece() != null
                            && Board.getBlocks()[0].getPiece() != null
                            && Board.getBlocks()[0].getPiece().getColor() == Colors.WHITE
                            && Board.getBlocks()[from].getPiece().getColor() == Colors.WHITE) {
                        if (Board.getBlocks()[0].getPiece().isFirstMove()) {
                            return true;
                        }
                        break;
                    }
                case 6:
                    if (Board.getBlocks()[4].getPiece() != null
                            && Board.getBlocks()[7].getPiece() != null
                            && Board.getBlocks()[7].getPiece().getColor() == Colors.WHITE
                            && Board.getBlocks()[from].getPiece().getColor() == Colors.WHITE) {
                        if (Board.getBlocks()[7].getPiece().isFirstMove()) {
                            return true;
                        }
                        break;
                    }
                case 58:
                    if (Board.getBlocks()[60].getPiece() != null
                            && Board.getBlocks()[56].getPiece() != null
                            && Board.getBlocks()[56].getPiece().getColor() == Colors.BLACK
                            && Board.getBlocks()[from].getPiece().getColor() == Colors.BLACK) {
                        if (Board.getBlocks()[56].getPiece().isFirstMove()) {
                            return true;
                        }
                        break;
                    }
                case 62:
                    if (Board.getBlocks()[60].getPiece() != null
                            && Board.getBlocks()[63].getPiece() != null
                            && Board.getBlocks()[63].getPiece().getColor() == Colors.BLACK
                            && Board.getBlocks()[from].getPiece().getColor() == Colors.BLACK) {
                        if (Board.getBlocks()[63].getPiece().isFirstMove()) {
                            return true;
                        }
                        break;
                    }
            }
        }
        return false;

    }
}