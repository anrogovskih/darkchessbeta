package org.darkchess.piece;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.general.Move;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;

import java.util.ArrayList;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Pawn extends Piece {



    public Pawn(Colors color, int blockIndex, boolean isOnStartPosition) {
        super(color, blockIndex, isOnStartPosition, 1);
    }

    @Override
    public boolean isValidMove(int from, int to) {
        if (!Board.noPieceOnWay(from,to)) {
            return false;
        }
        if (Board.getBlocks()[to].getPiece() != null && Board.getBlocks()[from].getPiece().getColor() == Board.getBlocks()[to].getPiece().getColor())
        {
            return false;
        }

        // this part of method allows pawn to capture pieces of opposite color diagonally and
        // forbids pawn to capture them on vertical.
        if (color == Colors.WHITE) {
            if ((to - from == 9 || to - from == 7) && Board.isInDiagonal(from, to) && Board.getBlocks()[to].getPiece() != null)  return true;
        } else {
            if ((from - to == 9 || from - to == 7) && Board.isInDiagonal(from, to) && Board.getBlocks()[to].getPiece() != null)  return true;
        }

        //this part refers only to En Passant possibility.
        if (color == Colors.WHITE) {
            if (Board.isInDiagonal(from, to) && isEnPassant(Game.lastMove))
                if ((isValidMove_EP(from-1,Colors.BLACK))
                        && (to - from == 7))
                    return true;
                else if ((isValidMove_EP(from+1,Colors.BLACK))
                        && (to - from == 9))
                    return true;
        } else {
            if (Board.isInDiagonal(from, to) && isEnPassant(Game.lastMove))
                if((isValidMove_EP(from-1, Colors.WHITE))
                        && (from - to == 9))
                    return true;
                else if((isValidMove_EP(from+1,Colors.WHITE))
                        && (from - to == 7))
                    return true;
        }

        if (color == Colors.WHITE) {
            if (((to - from == 8) || (from < 16 && to - from == 16)) && Board.getBlocks()[to].getPiece() == null) return true;
        } else {
            if (((from - to == 8) || (from > 47 && from - to == 16)) && Board.getBlocks()[to].getPiece() == null) return true;
        }

        return false;
    }

    @Override
    public String getType() {
        return "Pawn";
    }

    @Override
    public ArrayList<Integer> getVisibleBlocks(int position) {
        ArrayList<Integer> blocks = super.getVisibleBlocks(position);

        if (Board.getBlocks()[position].getPiece().color == Colors.WHITE && position < 16) {
            blocks.add(position + 16);
        } else if (Board.getBlocks()[position].getPiece().color == Colors.BLACK && position > 47) {
            blocks.add(position - 16);
        }

        return blocks;
    }
    public static void promotion(int position) {
        Piece piece = Board.getBlocks()[position].getPiece();
        if (piece.getType().equals("Pawn") && position > 55)
        {
            Board.getBlocks()[position].setPiece(null);
            Board.getBlocks()[position].setPiece(new Queen(Colors.WHITE, position, true));
        }
        if (piece.getType().equals("Pawn") && position < 8)
        {
            Board.getBlocks()[position].setPiece(null);
            Board.getBlocks()[position].setPiece(new Queen(Colors.BLACK, position, true));
        }
    }
    public static boolean isEnPassant (Move lastmove)
    {
        if(Board.numOfLinesInBetween(lastmove.getFrom(), lastmove.getTo()) == 2
                && Board.getBlocks()[lastmove.getTo()].getPiece() != null
                && Board.getBlocks()[lastmove.getTo()].getPiece().getType().equals("Pawn"))
        {
            return true;
        }
        return false;
    }
    public static void enPassant (Move move)
    {
        if (isEnPassant(Game.lastMove) && Board.getBlocks()[move.getTo()].getPiece().getType().equals("Pawn"))
        {
            if (Board.getBlocks()[move.getTo()].getPiece().getColor() == Colors.BLACK) {
                if (Board.getBlocks()[move.getTo() + 8].getPiece() != null
                        && Board.getBlocks()[move.getTo() + 8].getPiece().getType().equals("Pawn"))
                    Board.getBlocks()[move.getTo() + 8].setPiece(null);
            }
            else if (Board.getBlocks()[move.getTo()].getPiece().getColor() == Colors.WHITE)
                if(Board.getBlocks()[move.getTo()-8].getPiece() != null
                        && Board.getBlocks()[move.getTo()-8].getPiece().getType().equals("Pawn"))
                    Board.getBlocks()[move.getTo()-8].setPiece(null);
        }
    }
    private static boolean isValidMove_EP (int from, Colors color)
    {
        return (Board.getBlocks()[from].getPiece() != null
                && (Board.getBlocks()[from].getPiece().getColor() == color && Board.getBlocks()[from].getPiece().getType().equals("Pawn")));
    }
}
