package org.darkchess.player;

import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.general.Move;
import org.darkchess.network.Client;

import java.io.IOException;

/**
 * Created by Анатолий on 09.03.2016.
 */
public class NetworkPlayer extends Player{
    static Client client;

    public NetworkPlayer(Colors color) throws IOException {
        super(color);
        client = new Client();
        if (color == Colors.WHITE) {
            Move();
        }
    }

    @Override
    public boolean Move() {
        if (Game.lastMove.getFrom() != -1 && Game.lastMove.getTo() != -1) {
            try {
                System.out.println(color);
                sendMove(Game.lastMove);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Move opponentMove = null;
        while (opponentMove == null) {
            try {
                Thread.sleep(1000);
                System.out.println("nothing done");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                opponentMove = getMove();
                System.out.println("null");
                if (opponentMove != null) {
                    System.out.println(opponentMove.getFrom());
                    Game.makeMove(opponentMove);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            nullMove();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public void sendMove(Move move) throws IOException {
        if (color == Colors.WHITE) client.setP2Move(move);
        if (color == Colors.BLACK) client.setP1Move(move);
    }

    public void nullMove() throws IOException {
        if (color == Colors.BLACK) client.setP1Move(new Move(-1, -1));
        if (color == Colors.WHITE) client.setP2Move(new Move(-1, -1));
    }

    public Move getMove() throws IOException {
        if (color == Colors.WHITE) {
            return client.getP1Move();
        } else return client.getP2Move();
    }
}
