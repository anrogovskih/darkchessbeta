package org.darkchess.player;

import org.darkchess.general.Colors;
import org.darkchess.general.Colors;

/**
 * Created by Анатолий on 21.02.2016.
 */
public abstract class Player {

    Colors color;

    public Player(Colors color) {
        this.color = color;
    }

    public abstract boolean Move();

    public Colors getColor() {
        return color;
    }
}
