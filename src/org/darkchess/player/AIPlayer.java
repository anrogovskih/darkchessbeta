package org.darkchess.player;

import org.darkchess.ai.AI;
import org.darkchess.general.Colors;

/**
 * Created by Анатолий on 01.03.2016.
 */
public class AIPlayer extends Player {
    AI ai;

    public AIPlayer(Colors color) {
        super(color);
        this.ai = new AI(color);
    }

    @Override
    public boolean Move() {
        ai.move();
        return true;
    }
}
