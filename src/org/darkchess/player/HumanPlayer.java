package org.darkchess.player;

import org.darkchess.general.Colors;
import org.darkchess.general.Colors;

/**
 * Created by Анатолий on 21.02.2016.
 */
public class HumanPlayer extends Player {

    public HumanPlayer(Colors color) {
        super(color);
    }

    @Override
    public boolean Move() {
        return false;
    }
}
