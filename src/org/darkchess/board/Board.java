package org.darkchess.board;

import org.darkchess.general.Colors;
import org.darkchess.general.Colors;
import org.darkchess.piece.*;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Board {
    private static Block[] blocks = new Block[64];

    public Board() {
        for (int i = 0; i < 64; i++) {
            blocks[i] = new Block();
        }

        for (int i = 8; i < 16; i++) {
            blocks[i].setPiece(new Pawn(Colors.WHITE, i, true));
        }

        blocks[0].setPiece(new Rook(Colors.WHITE, 0, true));
        blocks[1].setPiece(new Knight(Colors.WHITE, 1, true));
        blocks[2].setPiece(new Bishop(Colors.WHITE, 2, true));
        blocks[3].setPiece(new Queen(Colors.WHITE, 3, true));
        blocks[4].setPiece(new King(Colors.WHITE, 4, true));
        blocks[5].setPiece(new Bishop(Colors.WHITE, 5, true));
        blocks[6].setPiece(new Knight(Colors.WHITE, 6, true));
        blocks[7].setPiece(new Rook(Colors.WHITE, 7, true));

        for (int i = 48; i < 56; i++) {
            blocks[i].setPiece(new Pawn(Colors.BLACK, i, true));
        }

        blocks[56].setPiece(new Rook(Colors.BLACK, 56, true));
        blocks[57].setPiece(new Knight(Colors.BLACK, 57, true));
        blocks[58].setPiece(new Bishop(Colors.BLACK, 58, true));
        blocks[59].setPiece(new Queen(Colors.BLACK, 59, true));
        blocks[60].setPiece(new King(Colors.BLACK, 60, true));
        blocks[61].setPiece(new Bishop(Colors.BLACK, 61, true));
        blocks[62].setPiece(new Knight(Colors.BLACK, 62, true));
        blocks[63].setPiece(new Rook(Colors.BLACK, 63, true));
    }

    public static Block[] getBlocks() {
        return blocks;
    }

    public static boolean isGameOver() {
        Piece whiteKing = null;
        Piece blackKing = null;

        for (Block block : blocks) {
            if (block.getPiece() != null) {
                if (block.getPiece().getClass() == King.class) {
                    if (block.getPiece().getColor() == Colors.WHITE) {
                        whiteKing = block.getPiece();
                    } else blackKing = block.getPiece();
                }
            }
        }

        if (whiteKing == null || blackKing == null) {
            return true;
        }

        return false;
    }

    // checks whether two blocks on the board are in one line.
    public static boolean isInLine(int startX, int endX) {
        for (int i = startX + 1; i <= endX; i++) {
            if (i % 8 == 0) {
                return false;
            }
        }
        return true;
    }

    // check whether piece on way
    public static boolean noPieceOnWay(int startX, int endX) {
        int toFrom = Math.abs(endX - startX);
        if (startX > endX) {
            int temp = startX;
            startX = endX;
            endX = temp;
        }

        //diagonal check
        if (toFrom % 7 == 0 && toFrom %8 != 0) {
            for (int i = startX + 7; i < endX; i += 7) {
                if (blocks[i].getPiece() != null) {
                    return false;
                }
            }
        }
        if (toFrom % 9 == 0) {
            for (int i = startX + 9; i < endX; i += 9) {
                if (blocks[i].getPiece() != null) {
                    return false;
                }
            }
        }

        //vertical check
        if (toFrom % 8 == 0) {{
            for (int i = startX + 8; i < endX; i += 8) {
                if (blocks[i].getPiece() != null) {
                    return false;
                }
            }
        }
        }

        //horizontal check
        if (toFrom < 8 && isInLine(startX, endX)) {
            for (int i = startX + 1; i < endX; i++) {
                if (blocks[i].getPiece() != null) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isInDiagonal(int startX, int endX) {
        return (numOfColumnsInBetween(startX, endX) == numOfLinesInBetween(startX, endX));
    }
    public static int numOfColumnsInBetween(int from, int to) {return Math.abs((to%8) - (from%8));}
    public static int numOfLinesInBetween(int from, int to)   {return Math.abs((from/8) - (to/8));}




}
