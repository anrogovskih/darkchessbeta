package org.darkchess.board;

import org.darkchess.piece.Piece;
import org.darkchess.piece.Piece;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Block {
    Piece piece = null;

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}
