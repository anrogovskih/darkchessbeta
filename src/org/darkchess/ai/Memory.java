package org.darkchess.ai;

import java.util.HashMap;

/**
 * Created by Анатолий on 01.03.2016.
 */
public class Memory {
    static HashMap<Integer, String> piecesPos;

    static {
        for (int i = 8; i < 16; i++) {
            piecesPos.put(i, "Pawn");
        }

        piecesPos.put(0, "Rook");
        piecesPos.put(7, "Rook");
        piecesPos.put(1, "Knight");
        piecesPos.put(2, "Bishop");
        piecesPos.put(3, "Queen");
        piecesPos.put(4, "King");
        piecesPos.put(5, "Bishop");
        piecesPos.put(6, "Knigh");
        piecesPos.put(7, "Rook");

    }
}
