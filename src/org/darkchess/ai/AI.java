package org.darkchess.ai;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.general.Move;

/**
 * Created by Анатолий on 01.03.2016.
 */
public class AI {
    Colors color;

    public AI(Colors color) {
        this.color = color;
    }

    public void move() {
        Vision.Look(color);
        Move move = getMove();
        Game.makeMove(move);
    }

    public Move getMove() {
        if (MoveGenerator.killKing(Vision.ownPiecesPos, Vision.enemyPiecesPos)) return MoveGenerator.move;
        if (MoveGenerator.saveKing(Vision.ownPiecesPos, Vision.enemyPiecesPos)) return MoveGenerator.move;
        if (MoveGenerator.savePieces(Vision.ownPiecesPos, Vision.enemyPiecesPos)) return MoveGenerator.move;
        if (MoveGenerator.attack(Vision.ownPiecesPos, Vision.enemyPiecesPos)) return MoveGenerator.move;
        if (MoveGenerator.safeMove(Vision.ownPiecesPos, Vision.enemyPiecesPos)) return MoveGenerator.move;
        return MoveGenerator.simpleMove(Vision.enemyPiecesPos);

    }
}
