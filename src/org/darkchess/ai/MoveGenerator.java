package org.darkchess.ai;

import org.darkchess.board.Board;
import org.darkchess.general.Move;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Анатолий on 01.03.2016.
 */
public class MoveGenerator {
    static Move move;

    public static ArrayList<Integer> getBlocks() {
        ArrayList<Integer> blocks = new ArrayList<>();
        for (int i = 0; i < 64; i++) blocks.add(i);
        Collections.shuffle(blocks);
        return blocks;
    }

    public static boolean saveKing(ArrayList<Integer> ownPiecesPos, ArrayList<Integer> enemyPiecesPos) {
        int kingPos = -1;

        for (Integer pos : ownPiecesPos) {
            if (Board.getBlocks()[pos].getPiece().getType().equals("King")) {
                kingPos = pos;
                break;
            }
        }

        for (Integer enemyPos : enemyPiecesPos) {
            if (Board.getBlocks()[enemyPos].getPiece().isValidMove(enemyPos, kingPos)) {
                for (Integer ownPiece : ownPiecesPos) {
                    if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, enemyPos)) {
                        move = new Move(ownPiece, enemyPos);
                        return true;
                    }
                }

                for (int pos : getBlocks()) {
                    if (Board.getBlocks()[kingPos].getPiece().isValidMove(kingPos, pos)) {
                        move = new Move(kingPos, pos);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static boolean savePieces(ArrayList<Integer> ownPiecesPos, ArrayList<Integer> enemyPiecesPos) {

        // queen
        for (Integer ownPiece : ownPiecesPos) {
            if (Board.getBlocks()[ownPiece].getPiece().getPower() == 4) {
                if (isBlockUnderAttack(ownPiece, enemyPiecesPos)) {
                    for (int pos : getBlocks()) {
                        if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, pos)
                                && !isBlockUnderAttack(pos, enemyPiecesPos)) {
                            move = new Move(ownPiece, pos);
                            return true;
                        }
                    }
                }
            }
        }

        // rook
        for (Integer ownPiece : ownPiecesPos) {
            if (Board.getBlocks()[ownPiece].getPiece().getPower() == 3) {
                if (isBlockUnderAttack(ownPiece, enemyPiecesPos)) {
                    for (int pos : getBlocks()) {
                        if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, pos)
                                && !isBlockUnderAttack(pos, enemyPiecesPos)) {
                            move = new Move(ownPiece, pos);
                            return true;
                        }
                    }
                }
            }
        }

        // king ang bishop
        for (Integer ownPiece : ownPiecesPos) {
            if (Board.getBlocks()[ownPiece].getPiece().getPower() == 2) {
                if (isBlockUnderAttack(ownPiece, enemyPiecesPos)) {
                    for (int pos : getBlocks()) {
                        if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, pos)
                                && !isBlockUnderAttack(pos, enemyPiecesPos)) {
                            move = new Move(ownPiece, pos);
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean killKing(ArrayList<Integer> ownPiecesPos, ArrayList<Integer> enemyPiecesPos) {
        int kingPos = -1;

        for (Integer enemyPos : enemyPiecesPos) {
            if (Board.getBlocks()[enemyPos].getPiece().getType().equals("King")) {
                kingPos = enemyPos;
                break;
            }
        }

        if (kingPos == -1) {
            return false;
        }

        for (Integer ownPiece : ownPiecesPos) {
            if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, kingPos)) {
                move = new Move(ownPiece, kingPos);
                return true;
            }
        }

        return false;
    }

    public static boolean attack(ArrayList<Integer> ownPiecesPos, ArrayList<Integer> enemyPiecesPos) {
        for (Integer ownPiece : ownPiecesPos) {
            for (Integer enemyPiece : enemyPiecesPos) {
                if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, enemyPiece)
                        && Board.getBlocks()[enemyPiece].getPiece().getPower() >=
                        Board.getBlocks()[ownPiece].getPiece().getPower()) {
                    move = new Move(ownPiece, enemyPiece);
                    return true;
                }
            }
        }

        return false;
    }

    public static Move simpleMove(ArrayList<Integer> ownPiecesPos) {
        Move simpleMove = null;
        Collections.shuffle(ownPiecesPos);
        for (Integer ownPiece : ownPiecesPos) {
            for (int pos : getBlocks()) {
                if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, pos)) {
                    simpleMove = new Move(ownPiece, pos);
                }
            }
        }
        return simpleMove;
    }

    public static boolean safeMove(ArrayList<Integer> ownPiecesPos, ArrayList<Integer> enemyPiecesPos) {
        Collections.shuffle(ownPiecesPos);
        for (Integer ownPiece : ownPiecesPos) {
            for (int pos : getBlocks()) {
                if (Board.getBlocks()[ownPiece].getPiece().isValidMove(ownPiece, pos)
                        && !isBlockUnderAttack(pos, enemyPiecesPos)
                        && Board.getBlocks()[pos].getPiece() == null) {
                    move = new Move(ownPiece, pos);
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isBlockUnderAttack(int piecePos, ArrayList<Integer> enemyPiecesPos) {
        for (Integer ePos : enemyPiecesPos) {
            if (Board.getBlocks()[ePos].getPiece().isValidMove(ePos, piecePos)) {
                return true;
            }
        }

        return false;
    }
}
