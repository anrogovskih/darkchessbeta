package org.darkchess.ai;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.piece.Piece;

import java.util.ArrayList;

/**
 * Created by Анатолий on 01.03.2016.
 */
public class Vision {
    static ArrayList<Integer> visibleBlocks;
    static ArrayList<Integer> ownPiecesPos;
    static ArrayList<Integer> enemyPiecesPos;

    public static void Look(Colors color) {
        visibleBlocks = getVisibleBlocks(color);
        ownPiecesPos = getOwnPieces(color);
        enemyPiecesPos = getEnemyPieces(color);
    }

    public static ArrayList<Integer> getEnemyPieces(Colors color) {
        ArrayList<Integer> pieces = new ArrayList<>();

        for (Integer block : visibleBlocks) {
            Piece piece = Board.getBlocks()[block].getPiece();
            if (piece != null && piece.getColor() != color) {
                pieces.add(block);
            }
        }

        return pieces;
    }

    public static ArrayList<Integer> getOwnPieces(Colors color) {
        ArrayList<Integer> pieces = new ArrayList<>();

        for (Integer block : visibleBlocks) {
            Piece piece = Board.getBlocks()[block].getPiece();
            if (piece != null && piece.getColor() == color) {
                pieces.add(block);
            }
        }

        return pieces;
    }

    public static ArrayList<Integer> getVisibleBlocks(Colors color) {
        ArrayList<Integer> blocks = new ArrayList<Integer>();

        int blockIndex = 0;
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                if (Board.getBlocks()[blockIndex].getPiece() != null) {
                    if (Board.getBlocks()[blockIndex].getPiece().getColor() == color) {
                        for (Integer block : Board.getBlocks()[blockIndex].getPiece().getVisibleBlocks(blockIndex)) {
                            {
                                if (block > -1 && block < 64) {
                                    blocks.add(block);
                                }
                            }
                        }
                    }
                }
                blockIndex = blockIndex + 1;
            }
        }
        return blocks;
    }
}
