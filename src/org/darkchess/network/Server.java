package org.darkchess.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Анатолий on 08.03.2016.
 */
public class Server extends Thread {
    protected Socket clientSocket;

    volatile static int p1from = -1;
    volatile static int p1to = -1;
    volatile static int p2from = -1;
    volatile static int p2to = -1;

    public static void main(String[] args) throws IOException
    {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(10008);
            System.out.println ("Connection Socket Created");
            try {
                while (true)
                {
                    System.out.println ("Waiting for Connection");
                    new Server (serverSocket.accept());
                }
            }
            catch (IOException e)
            {
                System.err.println("Accept failed.");
                System.exit(1);
            }
        }
        catch (IOException e)
        {
            System.err.println("Could not listen on port: 10008.");
            System.exit(1);
        }
        finally
        {
            try {
                serverSocket.close();
            }
            catch (IOException e)
            {
                System.err.println("Could not close port: 10008.");
                System.exit(1);
            }
        }
    }

    private Server (Socket clientSoc)
    {

        clientSocket = clientSoc;
        start();
    }

    public void run()
    {
        System.out.println ("New Communication Thread Started");

        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
                    true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader( clientSocket.getInputStream()));

            String inputLine;

            while ((inputLine = in.readLine()).toLowerCase() != null)
            {
                System.out.println("Server get from client: " + inputLine);

                if (inputLine.startsWith("get")) {
                    if (inputLine.contains("p1from")) out.println(p1from);
                    if (inputLine.contains("p2from")) out.println(p2from);
                    if (inputLine.contains("p1to")) out.println(p1to);
                    if (inputLine.contains("p2to")) out.println(p2to);
                } else if (inputLine.startsWith("set")) {
                    if (inputLine.contains("p1from")) p1from = Integer.parseInt(inputLine.split("=")[1]);
                    if (inputLine.contains("p2from")) p2from = Integer.parseInt(inputLine.split("=")[1]);
                    if (inputLine.contains("p1to"))   p1to = Integer.parseInt(inputLine.split("=")[1]);
                    if (inputLine.contains("p2to"))   p2to = Integer.parseInt(inputLine.split("=")[1]);
                    out.println("ok");
                } else out.println("ok");

                if (inputLine.equals("Bye."))
                    break;
            }

            out.close();
            in.close();
            clientSocket.close();
        }
        catch (IOException e)
        {
            System.err.println("Problem with Communication Server");
            System.exit(1);
        }
    }
}
