package org.darkchess.network;

import com.sun.deploy.util.SessionState;
import org.darkchess.general.Move;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Анатолий on 08.03.2016.
 */
public class Client {
    PrintWriter out;
    BufferedReader in;
    String serverHostname;
    Socket echoSocket;
    public Client() {
        serverHostname = new String ("127.0.0.1");

        try {
            echoSocket = new Socket(serverHostname, 10008);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                    + "the connection to: " + serverHostname);
            System.exit(1);
        }
    }
    public void setP1Move(Move move) throws IOException {
        if (move == null) {
            out.println("setp1from=-1");
            in.readLine();
            out.println("setp1to=-1");
            in.readLine();
        } else {
            out.println("setp1from=" + String.valueOf(move.getFrom()));
            System.out.println(in.readLine());

            out.println("setp1to=" + String.valueOf(move.getTo()));
            System.out.println(in.readLine());
        }
    }
    public void setP2Move(Move move) throws IOException {
        out.println("setp2from=" + String.valueOf(move.getFrom()));
        System.out.println(in.readLine());

        out.println("setp2to=" + String.valueOf(move.getTo()));
        System.out.println(in.readLine());
    }

    public Move getP1Move() throws IOException {
        out.println("getp1from");
        int from = Integer.parseInt(in.readLine());

        out.println("getp1to");
        int to = Integer.parseInt(in.readLine());

        if (from < 0) {
            return null;
        }
        return new Move(from, to);
    }

    public Move getP2Move() throws IOException {
        out.println("getp2from");
        int from = Integer.parseInt(in.readLine());

        out.println("getp2to");
        int to = Integer.parseInt(in.readLine());

        if (from < 0) {
            return null;
        }

        return new Move(from, to);
    }

    public void send(String s) throws IOException {
        out.println(s);
        System.out.println(in.readLine());
    }

}
