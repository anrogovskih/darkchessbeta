package org.darkchess.network;

import org.darkchess.general.Move;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Анатолий on 08.03.2016.
 */
public class ClientTest {
    public static void main(String args[]) throws IOException {
        Client client = new Client();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            reader.readLine();
            Move move = new Move(63, 41);
            if (move != null) {
//                System.out.println(move.getFrom());
//                System.out.println(move.getTo());
                client.setP2Move(move);
            } else {
                System.out.println("no move");
            }

        }
    }
}
