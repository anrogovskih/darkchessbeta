package org.darkchess.general;

import org.darkchess.board.Board;
import org.darkchess.gui.ChessGUI;
import org.darkchess.piece.King;
import org.darkchess.piece.Pawn;
import org.darkchess.player.AIPlayer;
import org.darkchess.player.HumanPlayer;
import org.darkchess.player.NetworkPlayer;
import org.darkchess.player.Player;
import org.darkchess.gui.ChessGUI;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Game {
    public Board board;
    static ChessGUI chessGUI;
    public static Player opponent;
    public static Player player;
    public static Move lastMove = new Move(-1, -1);

    public static void main(String[] args) throws InterruptedException, IOException {
        new Game(new Board());
    }

    public Game(Board board) throws InterruptedException  {
        this.board = board;
        chessGUI = new ChessGUI(board);
        chessGUI.updateUI();
    }

    public static void run() throws IOException {
        Board board = new Board();
        chessGUI.setIsWhiteMove(true);
        setOpponent();
        chessGUI.createPieces();
        chessGUI.repaint();
    }

    public static void movePiece (Move move)
    {
        Board.getBlocks()[move.getFrom()].getPiece().setIsFirstMove(false);
        Board.getBlocks()[move.getFrom()].getPiece().setBlockIndex(move.getTo());
        Board.getBlocks()[move.getTo()].setPiece(Board.getBlocks()[move.getFrom()].getPiece());
        Board.getBlocks()[move.getFrom()].setPiece(null);
    }

    public static void makeMove(Move move) {
        movePiece(move);
        King.doCastling(move);
        Pawn.promotion(move.getTo());
        Pawn.enPassant(move);
        lastMove = move;
        chessGUI.setIsWhiteMove(!chessGUI.getIsWhiteMove());}

    public static void crazyGame() throws IOException {
        while (!Board.isGameOver()) {
            try {
                player.Move();
                chessGUI.createAllPieces();
                chessGUI.repaint();
                chessGUI.setIsWhiteMove(false);
                Thread.sleep(300);
                if (Board.isGameOver()) run();

                opponent.Move();
                chessGUI.createAllPieces();
                chessGUI.repaint();
                chessGUI.setIsWhiteMove(true);
                Thread.sleep(300);
                if (Board.isGameOver()) run();
            } catch (InterruptedException e) { }
        }
    }

    public static void setOpponent() throws IOException {
        Object[] options = {"Human",
                "Computer", "Network game (White)", "Network game (Black)", "Crazy Game"};
        int n = JOptionPane.showOptionDialog(chessGUI,
                "Choose your opponent",
                "Start new game",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[0]); //default button title

        if (n == 0) {
            player = new HumanPlayer(Colors.WHITE);
            opponent = new HumanPlayer(Colors.BLACK);
        }
        if (n == 1) {
            player = new HumanPlayer(Colors.WHITE);
            opponent = new AIPlayer(Colors.BLACK);
        }
        if (n == 2) {
            player = new HumanPlayer(Colors.WHITE);
            opponent = new NetworkPlayer(Colors.BLACK);
        }
        if (n == 3) {
            player = new HumanPlayer(Colors.BLACK);
            opponent = new NetworkPlayer(Colors.WHITE);
            chessGUI.setIsWhiteMove(false);
        }
        if (n == 4) {
            player = new AIPlayer(Colors.WHITE);
            opponent = new AIPlayer(Colors.BLACK);
            crazyGame();
        }
    }
}
