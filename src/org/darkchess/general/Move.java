package org.darkchess.general;

/**
 * Created by Анатолий on 17.02.2016.
 */
public class Move {
    int to;
    int from;

    public Move(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
}
