package org.darkchess.gui;

import org.darkchess.general.Colors;
import org.darkchess.piece.Piece;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by Анатолий on 18.02.2016.
 */
public class PieceGUI {
    private Image img;
    private int x;
    private int y;
    private Piece piece;

    public PieceGUI(Piece piece, int blockIndex) {

        // set image for piece
        String name = "/org/darkchess/gui/img/";
        switch (piece.getType()) {
            case "Bishop":
                name += piece.getColor() == Colors.WHITE ? "wb" : "bb";
                break;
            case "King":
                name += piece.getColor() == Colors.WHITE ? "wk" : "bk";
                break;
            case "Knight":
                name += piece.getColor() == Colors.WHITE ? "wn" : "bn";
                break;
            case "Pawn":
                name += piece.getColor() == Colors.WHITE ? "wp" : "bp";
                break;
            case "Queen":
                name += piece.getColor() == Colors.WHITE ? "wq" : "bq";
                break;
            case "Rook":
                name += piece.getColor() == Colors.WHITE ? "wr" : "br";
                break;
        }
        URL urlBackgroundImg = getClass().getResource(name + ".png");
        this.img = new ImageIcon(urlBackgroundImg).getImage();

        this.x = ChessGUI.convertBlockToX(blockIndex);
        this.y = ChessGUI.convertBlockToY(blockIndex);

        this.piece = piece;
    }

    public void resetToUnderlyingPiecePosition() {
        this.x = ChessGUI.convertColToX(ChessGUI.convertXToCol(this.x));
        this.y = ChessGUI.convertRowToY(ChessGUI.convertYToRow(this.y));
    }

    public Image getImg() {
        return img;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return img.getHeight(null);
    }

    public int getHeight() {
        return img.getHeight(null);
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }
}
