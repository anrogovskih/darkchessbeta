package org.darkchess.gui;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.general.Move;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.List;

/**
 * Created by Анатолий on 18.02.2016.
 */
public class PiecesDragAndDropListener implements MouseListener, MouseMotionListener {

    private List<PieceGUI> pieces;
    private ChessGUI chessGui;

    private PieceGUI dragPiece;
    private int dragOffsetX;
    private int dragOffsetY;


    public PiecesDragAndDropListener(List<PieceGUI> pieces, ChessGUI chessGui) {
        this.pieces = pieces;
        this.chessGui = chessGui;
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        int x = evt.getPoint().x;
        int y = evt.getPoint().y;

        // find out which piece to move.
        // we check the list from top to bottom
        // (therefore we iterate in reverse order)
        //
        for (int i = this.pieces.size()-1; i >= 0; i--) {
            PieceGUI piece = this.pieces.get(i);
            Colors currentColor = chessGui.getIsWhiteMove() ? Colors.WHITE : Colors.BLACK;

            if (piece.getPiece().getColor() == currentColor) {

                if( mouseOverPiece(piece,x,y)){

                    int blockIndex = chessGui.convertXYToBlock(x,y);
                    if (chessGui.isOptionsIsHighlightedBlocks()) {
                        chessGui.getMoves(blockIndex);
                    }
                    chessGui.repaint();
                    // calculate offset, because we do not want the drag piece
                    // to jump with it's upper left corner to the current mouse
                    // position
                    //
                    this.dragOffsetX = x - piece.getX();
                    this.dragOffsetY = y - piece.getY();
                    this.dragPiece = piece;
                    break;
                }
            }
        }

        // move drag piece to the top of the list
        if(this.dragPiece != null){
            this.pieces.remove( this.dragPiece );
            this.pieces.add(this.dragPiece);
        }
    }

    /**
     * check whether the mouse is currently over this piece
     * @param piece the playing piece
     * @param x x coordinate of mouse
     * @param y y coordinate of mouse
     * @return true if mouse is over the piece
     */
    private boolean mouseOverPiece(PieceGUI piece, int x, int y) {
        return piece.getX() <= x
                && piece.getX()+piece.getWidth() >= x
                && piece.getY() <= y
                && piece.getY()+piece.getHeight() >= y;
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        chessGui.clearHighlightedBlocks();
        //when you drag a piece, this string gets information of its current block index
        int from = dragPiece.getPiece().getBlockIndex();

        // converts coordinates from the pixel, you drop a dragged piece to the block index
        int to = chessGui.convertXYToBlock(dragPiece.getX(), dragPiece.getY());

        // calls isValidMove method from a piece package
        boolean isValid = dragPiece.getPiece().isValidMove(from,to);

        if (isValid) {
            dragPiece.resetToUnderlyingPiecePosition();
            dragPiece.getPiece().setBlockIndex(to);

            Game.makeMove(new Move(from, to));

            if (Board.isGameOver()) {
                try {
                    Game.run();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (Game.opponent.Move()) {
                if (Game.opponent.getColor() == Colors.BLACK) {
                    chessGui.setIsWhiteMove(true);
                }
                else chessGui.setIsWhiteMove(false);
            } else {
                chessGui.clearPieces();
                chessGui.repaint();
                JOptionPane.showInternalMessageDialog(chessGui, null, "Your move", JOptionPane.OK_OPTION);
            }
            chessGui.createPieces();
        }   else {
            this.dragPiece.setX(ChessGUI.convertBlockToX(from));
            this.dragPiece.setY(ChessGUI.convertBlockToY(from));
        }
        this.chessGui.repaint();
        this.dragPiece = null;
    }

    @Override
    public void mouseDragged(MouseEvent evt) {
        if(this.dragPiece != null){
            this.dragPiece.setX(evt.getPoint().x - this.dragOffsetX);
            this.dragPiece.setY(evt.getPoint().y - this.dragOffsetY);
            this.chessGui.repaint();
        }
    }




    @Override
    public void mouseClicked(MouseEvent arg0) {}

    @Override
    public void mouseEntered(MouseEvent arg0) {}

    @Override
    public void mouseExited(MouseEvent arg0) {}

    @Override
    public void mouseMoved(MouseEvent arg0) {}

}
