package org.darkchess.gui;

import org.darkchess.board.Board;
import org.darkchess.general.Colors;
import org.darkchess.general.Game;
import org.darkchess.piece.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Анатолий on 18.02.2016.
 */
public class ChessGUI extends JPanel {

    private Image imgBackground;
    private ArrayList<PieceGUI> pieces = new ArrayList<PieceGUI>();
    private Board board;
    private boolean isWhiteMove = true;
    private ArrayList<Integer> highlightedBlocks = new ArrayList<>();
    private boolean optionsIsHighlightedBlocks = true;
    private boolean optionsIsDark = true;

    public boolean isOptionsIsHighlightedBlocks() {
        return optionsIsHighlightedBlocks;
    }

    public boolean isOptionsIsDark() {        return optionsIsDark;}

    void clearHighlightedBlocks() {
        highlightedBlocks.clear();
    }

    public boolean getIsWhiteMove() {
        return isWhiteMove;
    }

    public void setIsWhiteMove(boolean isWhite) {
        isWhiteMove = isWhite;
    }

    void clearPieces() {
        pieces.clear();
    }

    @Override
    protected void paintComponent(Graphics g) {
            g.drawImage(this.imgBackground, 0, 0, null);
            for (PieceGUI piece : this.pieces) {
                g.drawImage(piece.getImg(), piece.getX(), piece.getY(), null);
            }
        if (isOptionsIsHighlightedBlocks()) {
            for (Integer move : highlightedBlocks) {
                drawRect(move, g);
            }
        }
    }

    public void getMoves(int blockIndex) {
        Piece piece = Board.getBlocks()[blockIndex].getPiece();
        if (piece != null) {
            for (int i = 0; i < 63; i++) {
                if (piece.isValidMove(blockIndex, i)) {
                    highlightedBlocks.add(i);
                }
            }
        }
    }

    public void drawRect(int blockIndex, Graphics g) {
        g.setColor(Color.RED);

        int x = 5;
        int y = 4;
        float blockSize = 62.75f;

        int tempX = blockIndex - (blockIndex / 8 * 8) + 1;
        x = (int) (x + (tempX - 1) * blockSize);

        int tempY = blockIndex / 8 + 1;
        y = (int) (y + (8 - tempY) * blockSize);

        g.drawRect(x, y, 53, 53);
    }

    public ChessGUI(Board board) throws InterruptedException {

        JFrame frame = new JFrame();
        this.board = board;

        // background
        URL urlBackgroundImg = getClass().getResource("/org/darkchess/gui/img/bo.jpg");
        this.imgBackground = new ImageIcon(urlBackgroundImg).getImage();

        //drag and drop
        PiecesDragAndDropListener listener = new PiecesDragAndDropListener(this.pieces,
                this);
        this.addMouseListener(listener);
        this.addMouseMotionListener(listener);

        //menu
        final JMenuBar menuBar = new JMenuBar();
        final JButton mbNewGame = new JButton("New Game");
        mbNewGame.setBorderPainted(false);
        mbNewGame.addActionListener(e -> {
            try {
                Game.run();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            createPieces();
            mbNewGame.setFocusPainted(false);
        });
        final JButton mbAbout = new JButton("About");
        mbAbout.setBorderPainted(false);
        mbAbout.addActionListener(e -> {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI("https://en.wikipedia.org/wiki/Dark_chess"));
                mbAbout.setFocusPainted(false);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (URISyntaxException e1) {
                e1.printStackTrace();
            }
        });
        final JButton mbOptions = new JButton("Options");
        mbOptions.setBorderPainted(false);
        mbOptions.addActionListener(e ->{
            JFrame frame1 = new JFrame("Options");
            frame1.setLocationRelativeTo(frame);
            JPanel mainPanel = new JPanel();
            mainPanel.setLayout(new BorderLayout());
            JPanel optionsPanel = new JPanel ();
            frame1.setBounds(100,100,200,200);
            final JCheckBox highlightingBlocksBox = new JCheckBox("Подсветка возможных ходов");
            highlightingBlocksBox.setSelected(optionsIsHighlightedBlocks);
            optionsPanel.add(highlightingBlocksBox);
            highlightingBlocksBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED) {
                        optionsIsHighlightedBlocks = true;
                    } else {
                        optionsIsHighlightedBlocks = false;
                    }
                }
            });
            final JCheckBox isDarkBox = new JCheckBox("Туман войны");
            isDarkBox.setSelected(optionsIsDark);
            optionsPanel.add(isDarkBox);
            isDarkBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        optionsIsDark = true;
                    }
                    else {
                        optionsIsDark = false;
                    }
                }
            });
            mainPanel.add(optionsPanel);
            frame1.add(mainPanel);
            frame1.setLocationByPlatform(true);
            frame1.setVisible(true);
        });
        menuBar.add(mbNewGame);
        menuBar.add(mbAbout);
        menuBar.add(mbOptions);

        //creating frame
        frame.pack();
        frame.setSize(518 + getInsets().left + getInsets().right, 567 + getInsets().top + getInsets().bottom);
        frame.setVisible(true);
        frame.setComponentOrientation(null);
        frame.setTitle("Dark Chess");
        URL urlIcon = getClass().getResource("/org/darkchess/gui/img/icon.png");
        Image imgIcon = new ImageIcon(urlIcon).getImage();
        frame.setIconImage(imgIcon);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.add(this);

        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    public void createAllPieces() {
        this.pieces.clear();
        int blockIndex = 0;
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                if (Board.getBlocks()[blockIndex].getPiece() != null) {
                    this.pieces.add(new PieceGUI(Board.getBlocks()[blockIndex].getPiece(), blockIndex));
                }
                blockIndex = blockIndex + 1;
            }
        }
    }

    public void createPieces() {
        Colors currentColor = getIsWhiteMove() ? Colors.WHITE : Colors.BLACK;
        ArrayList<Integer> blocks = getVisibleBlocks(currentColor);
        this.pieces.clear();
        int blockIndex = 0;
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                if ((Board.getBlocks()[blockIndex].getPiece() != null) && isDark(blocks,blockIndex)) {
                    this.pieces.add(new PieceGUI(Board.getBlocks()[blockIndex].getPiece(), blockIndex));
                }
                blockIndex = blockIndex + 1;
            }
        }
        System.out.println(Board.getBlocks()[5].getPiece());
        System.out.println(Board.getBlocks()[7].getPiece());
    }
    private boolean isDark(ArrayList<Integer> blocks, int blockIndex){
        if (isOptionsIsDark()){
            return blocks.contains(blockIndex);
        }
        else return true;
    }

    public ArrayList<Integer> getVisibleBlocks(Colors color) {
        ArrayList<Integer> blocks = new ArrayList<Integer>();

        int blockIndex = 0;
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                if (Board.getBlocks()[blockIndex].getPiece() != null) {
                    if (Board.getBlocks()[blockIndex].getPiece().getColor() == color) {
                            for (Integer block : Board.getBlocks()[blockIndex].getPiece().getVisibleBlocks(blockIndex)) {
                                blocks.add(block);
                            }
                    }
                }
                blockIndex = blockIndex + 1;
            }
        }
        return blocks;
    }

    public static int convertBlockToX(int blockIndex){
        int startX = 5;
        float blockSize = 62.75f;

        int tempX = blockIndex - (blockIndex / 8 * 8) + 1;

        return (int) (startX + (tempX - 1) * blockSize);
    }

    public static int convertBlockToY(int blockIndex){
        int startY = 5;
        float blockSize = 62.75f;

        int tempY = blockIndex / 8 + 1;

        return  (int) (startY + (8 - tempY) * blockSize);
    }

    public static int convertColToX(int x){
        int startX = 5;
        float blockSize = 62.75f;

        return (int) (startX + (x - 1) * blockSize);
    }

    public static int convertRowToY(int y){
        int startY = 5;
        float blockSize = 62.75f;

        return  (int) (startY + (8 - y) * blockSize);
    }

    public static int convertXToCol(int x){
        return ((x + 20) / 62) + 1;
    }

    public static int convertYToRow(int y){
        return 8 - ((y + 20) / 62);
    }

    public int convertXYToBlock(int x, int y) {
        int col = ((x + 20) / 62) + 1;
        int row = 8 - ((y + 20) / 62);

        return (row - 1) * 8 + (col - 1);
    }


}

